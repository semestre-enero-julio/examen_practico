/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Juego_Gato;

import javax.swing.JOptionPane;

/**
 *
 * @author Dreed
 */
public class JuegoDelGato extends javax.swing.JFrame {

    private String equis = "X";
    private String cero = "O";

    public JuegoDelGato() {
        initComponents();

    }

    public void ganador() {
        String uno = Casilla_1_.getText();
        String dos = Casilla_2_.getText();
        String tres = Casilla_3_.getText();
        String cuatro = Casilla_4_.getText();
        String cinco = Casilla_5_.getText();
        String seis = Casilla_6_.getText();
        String siete = Casilla_7_.getText();
        String ocho = Casilla_8_.getText();
        String nueve = Casilla_9_.getText();

        if (uno.equals("X") && dos.equals("X") && tres.equals("X")) {
            GanadorX();
        }

        if (cuatro.equals("X") && cinco.equals("X") && seis.equals("X")) {
            GanadorX();
        }
        if (siete.equals("X") && ocho.equals("X") && nueve.equals("X")) {
            GanadorX();
        }
        if (uno.equals("X") && cuatro.equals("X") && siete.equals("X")) {
            GanadorX();
        }
        if (dos.equals("X") && cinco.equals("X") && ocho.equals("X")) {
            GanadorX();
        }
        if (tres.equals("X") && seis.equals("X") && nueve.equals("X")) {
            GanadorX();
        }
        if (uno.equals("X") && cinco.equals("X") && nueve.equals("X")) {
            GanadorX();
        }
        if (tres.equals("X") && cinco.equals("X") && siete.equals("X")) {
            GanadorX();
        }

        //Ganador O
        if (uno.equals("O") && dos.equals("O") && tres.equals("O")) {
            GanadorO();
        }

        if (cuatro.equals("O") && cinco.equals("O") && seis.equals("O")) {
            GanadorO();
        }
        if (siete.equals("O") && ocho.equals("O") && nueve.equals("O")) {
            GanadorO();
        }
        if (uno.equals("O") && cuatro.equals("O") && siete.equals("O")) {
            GanadorO();
        }
        if (dos.equals("O") && cinco.equals("O") && ocho.equals("O")) {
            GanadorO();
        }
        if (tres.equals("O") && seis.equals("O") && nueve.equals("O")) {
            GanadorO();
        }
        if (uno.equals("O") && cinco.equals("O") && nueve.equals("O")) {
            GanadorO();
        }
        if (tres.equals("O") && cinco.equals("O") && siete.equals("O")) {
            GanadorO();
        }

    }

    public void empate() {
        String uno = Casilla_1_.getText();
        String dos = Casilla_2_.getText();
        String tres = Casilla_3_.getText();
        String cuatro = Casilla_4_.getText();
        String cinco = Casilla_5_.getText();
        String seis = Casilla_6_.getText();
        String siete = Casilla_7_.getText();
        String ocho = Casilla_8_.getText();
        String nueve = Casilla_9_.getText();

        if (uno != "" && dos != "" && tres != "" && cuatro != "" && cinco != "" && seis != "" && siete != "" && ocho != "" && nueve != "") {
            JOptionPane.showMessageDialog(null, "El Juego Esta Empatado");
            nuevoJuego();
        }

    }

    public void GanadorX() {
        JOptionPane.showMessageDialog(null, "El Ganador Es La X");
        nuevoJuego();
    }

    public void GanadorO() {
        JOptionPane.showMessageDialog(null, "El Ganador Es La O");
        nuevoJuego();

    }

    public void nuevoJuego() {

        Casilla_1_.setText("");
        Casilla_2_.setText("");
        Casilla_3_.setText("");
        Casilla_4_.setText("");
        Casilla_5_.setText("");
        Casilla_6_.setText("");
        Casilla_7_.setText("");
        Casilla_8_.setText("");
        Casilla_9_.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        Casilla_1_ = new javax.swing.JButton();
        Casilla_2_ = new javax.swing.JButton();
        Casilla_3_ = new javax.swing.JButton();
        Casilla_4_ = new javax.swing.JButton();
        Casilla_5_ = new javax.swing.JButton();
        Casilla_6_ = new javax.swing.JButton();
        Casilla_7_ = new javax.swing.JButton();
        Casilla_8_ = new javax.swing.JButton();
        Casilla_9_ = new javax.swing.JButton();
        NuevoJuego_ = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new java.awt.GridLayout(3, 3));

        Casilla_1_.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        Casilla_1_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla_1_ActionPerformed(evt);
            }
        });
        jPanel1.add(Casilla_1_);

        Casilla_2_.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        Casilla_2_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla_2_ActionPerformed(evt);
            }
        });
        jPanel1.add(Casilla_2_);

        Casilla_3_.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        Casilla_3_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla_3_ActionPerformed(evt);
            }
        });
        jPanel1.add(Casilla_3_);

        Casilla_4_.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        Casilla_4_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla_4_ActionPerformed(evt);
            }
        });
        jPanel1.add(Casilla_4_);

        Casilla_5_.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        Casilla_5_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla_5_ActionPerformed(evt);
            }
        });
        jPanel1.add(Casilla_5_);

        Casilla_6_.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        Casilla_6_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla_6_ActionPerformed(evt);
            }
        });
        jPanel1.add(Casilla_6_);

        Casilla_7_.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        Casilla_7_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla_7_ActionPerformed(evt);
            }
        });
        jPanel1.add(Casilla_7_);

        Casilla_8_.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        Casilla_8_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla_8_ActionPerformed(evt);
            }
        });
        jPanel1.add(Casilla_8_);

        Casilla_9_.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        Casilla_9_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla_9_ActionPerformed(evt);
            }
        });
        jPanel1.add(Casilla_9_);

        NuevoJuego_.setText("Nuevo Juego");
        NuevoJuego_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NuevoJuego_ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(126, 126, 126)
                        .addComponent(NuevoJuego_)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(NuevoJuego_, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Casilla_1_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla_1_ActionPerformed

        Casilla_1_.setText(equis);
        if (equis.equals("X")) {
            equis = "O";

        } else {
            equis = "X";
        }
        ganador();
        empate();
    }//GEN-LAST:event_Casilla_1_ActionPerformed

    private void Casilla_3_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla_3_ActionPerformed
        // TODO add your handling code here:

        Casilla_3_.setText(equis);
        if (equis.equals("X")) {
            equis = "O";
        } else {
            equis = "X";
        }
        ganador();
        empate();
    }//GEN-LAST:event_Casilla_3_ActionPerformed

    private void Casilla_2_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla_2_ActionPerformed
        // TODO add your handling code here:

        Casilla_2_.setText(equis);
        if (equis.equals("X")) {
            equis = "O";
        } else {
            equis = "X";
        }
        ganador();
        empate();

    }//GEN-LAST:event_Casilla_2_ActionPerformed

    private void Casilla_4_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla_4_ActionPerformed
        // TODO add your handling code here:

        Casilla_4_.setText(equis);
        if (equis.equals("X")) {
            equis = "O";
        } else {
            equis = "X";
        }
        ganador();
        empate();

    }//GEN-LAST:event_Casilla_4_ActionPerformed

    private void Casilla_5_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla_5_ActionPerformed
        // TODO add your handling code here:

        Casilla_5_.setText(equis);
        if (equis.equals("X")) {
            equis = "O";
        } else {
            equis = "X";
        }
        ganador();
        empate();


    }//GEN-LAST:event_Casilla_5_ActionPerformed

    private void Casilla_6_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla_6_ActionPerformed
        // TODO add your handling code here:
        Casilla_6_.setText(equis);
        if (equis.equals("X")) {
            equis = "O";
        } else {
            equis = "X";
        }
        ganador();
        empate();
    }//GEN-LAST:event_Casilla_6_ActionPerformed

    private void Casilla_7_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla_7_ActionPerformed
        // TODO add your handling code here:

        Casilla_7_.setText(equis);
        if (equis.equals("X")) {
            equis = "O";
        } else {
            equis = "X";
        }
        ganador();
        empate();

    }//GEN-LAST:event_Casilla_7_ActionPerformed

    private void Casilla_8_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla_8_ActionPerformed
        // TODO add your handling code here:

        Casilla_8_.setText(equis);
        if (equis.equals("X")) {
            equis = "O";
        } else {
            equis = "X";
        }
        ganador();
        empate();

    }//GEN-LAST:event_Casilla_8_ActionPerformed

    private void Casilla_9_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla_9_ActionPerformed
        // TODO add your handling code here:

        Casilla_9_.setText(equis);
        if (equis.equals("X")) {
            equis = "O";
        } else {
            equis = "X";
        }
        ganador();
        empate();


    }//GEN-LAST:event_Casilla_9_ActionPerformed

    private void NuevoJuego_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NuevoJuego_ActionPerformed
        // TODO add your handling code here:

        Casilla_1_.setText("");
        Casilla_2_.setText("");
        Casilla_3_.setText("");
        Casilla_4_.setText("");
        Casilla_5_.setText("");
        Casilla_6_.setText("");
        Casilla_7_.setText("");
        Casilla_8_.setText("");
        Casilla_9_.setText("");
    }//GEN-LAST:event_NuevoJuego_ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JuegoDelGato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JuegoDelGato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JuegoDelGato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JuegoDelGato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JuegoDelGato().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Casilla_1_;
    private javax.swing.JButton Casilla_2_;
    private javax.swing.JButton Casilla_3_;
    private javax.swing.JButton Casilla_4_;
    private javax.swing.JButton Casilla_5_;
    private javax.swing.JButton Casilla_6_;
    private javax.swing.JButton Casilla_7_;
    private javax.swing.JButton Casilla_8_;
    private javax.swing.JButton Casilla_9_;
    private javax.swing.JButton NuevoJuego_;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
