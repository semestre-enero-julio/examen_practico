/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculadora_Simple;

/**
 *
 * @author Dreed
 */
public class CalculadoraSimple extends javax.swing.JFrame {

    public long digito1;
    public long digito2;
    public String funcion;
    public double aux;

    public CalculadoraSimple() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        Visor = new javax.swing.JLabel();
        Numero_1 = new javax.swing.JButton();
        Numero_2 = new javax.swing.JButton();
        Suma = new javax.swing.JButton();
        Numero_4 = new javax.swing.JButton();
        Numero_3 = new javax.swing.JButton();
        Numero_6 = new javax.swing.JButton();
        Numero_5 = new javax.swing.JButton();
        Numero_7 = new javax.swing.JButton();
        Numero_8 = new javax.swing.JButton();
        Decimal = new javax.swing.JButton();
        Numero_0 = new javax.swing.JButton();
        Borrrar = new javax.swing.JButton();
        Numero_9 = new javax.swing.JButton();
        Multiplicacion = new javax.swing.JButton();
        Division = new javax.swing.JButton();
        Igual = new javax.swing.JButton();
        Resta = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(51, 51, 255));

        Visor.setOpaque(true);

        Numero_1.setText("1");
        Numero_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Numero_1ActionPerformed(evt);
            }
        });

        Numero_2.setText("2");
        Numero_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Numero_2ActionPerformed(evt);
            }
        });

        Suma.setText("+");
        Suma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SumaActionPerformed(evt);
            }
        });

        Numero_4.setText("4");
        Numero_4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Numero_4ActionPerformed(evt);
            }
        });

        Numero_3.setText("3");
        Numero_3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Numero_3ActionPerformed(evt);
            }
        });

        Numero_6.setText("6");
        Numero_6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Numero_6ActionPerformed(evt);
            }
        });

        Numero_5.setText("5");
        Numero_5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Numero_5ActionPerformed(evt);
            }
        });

        Numero_7.setText("7");
        Numero_7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Numero_7ActionPerformed(evt);
            }
        });

        Numero_8.setText("8");
        Numero_8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Numero_8ActionPerformed(evt);
            }
        });

        Decimal.setText(".");
        Decimal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DecimalActionPerformed(evt);
            }
        });

        Numero_0.setText("0");
        Numero_0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Numero_0ActionPerformed(evt);
            }
        });

        Borrrar.setText("C");
        Borrrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BorrrarActionPerformed(evt);
            }
        });

        Numero_9.setText("9");
        Numero_9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Numero_9ActionPerformed(evt);
            }
        });

        Multiplicacion.setText("*");
        Multiplicacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MultiplicacionActionPerformed(evt);
            }
        });

        Division.setText("/");
        Division.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DivisionActionPerformed(evt);
            }
        });

        Igual.setText("=");
        Igual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IgualActionPerformed(evt);
            }
        });

        Resta.setText("-");
        Resta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RestaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(Resta, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Numero_4, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(Suma, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Numero_1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(Multiplicacion, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Numero_7, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(Division, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Decimal, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(Numero_2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(Numero_3, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(Numero_5, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Numero_6, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(Numero_8, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Numero_9, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(Numero_0, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Borrrar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(Visor, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(Igual, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(97, 97, 97))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Visor, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Numero_1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Numero_2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Suma, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Numero_3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Numero_4, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Numero_5, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Resta, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(Numero_6, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Numero_7, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Numero_8, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Numero_9, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Multiplicacion, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Numero_0, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Borrrar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Decimal, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Division, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Igual, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Numero_4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Numero_4ActionPerformed
        // TODO add your handling code here:
        this.Visor.setText(this.Visor.getText() + "4");
    }//GEN-LAST:event_Numero_4ActionPerformed

    private void Numero_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Numero_1ActionPerformed
        // TODO add your handling code here:
        this.Visor.setText(this.Visor.getText() + "1");
    }//GEN-LAST:event_Numero_1ActionPerformed

    private void Numero_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Numero_2ActionPerformed
        // TODO add your handling code here:
        this.Visor.setText(this.Visor.getText() + "2");
    }//GEN-LAST:event_Numero_2ActionPerformed

    private void Numero_3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Numero_3ActionPerformed
        // TODO add your handling code here:
        this.Visor.setText(this.Visor.getText() + "3");
    }//GEN-LAST:event_Numero_3ActionPerformed

    private void Numero_6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Numero_6ActionPerformed
        // TODO add your handling code here:
        this.Visor.setText(this.Visor.getText() + "6");
    }//GEN-LAST:event_Numero_6ActionPerformed

    private void Numero_5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Numero_5ActionPerformed
        // TODO add your handling code here:
        this.Visor.setText(this.Visor.getText() + "5");
    }//GEN-LAST:event_Numero_5ActionPerformed

    private void Numero_7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Numero_7ActionPerformed
        // TODO add your handling code here:
        this.Visor.setText(this.Visor.getText() + "7");
    }//GEN-LAST:event_Numero_7ActionPerformed

    private void Numero_8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Numero_8ActionPerformed
        // TODO add your handling code here:
        this.Visor.setText(this.Visor.getText() + "8");
    }//GEN-LAST:event_Numero_8ActionPerformed

    private void DecimalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DecimalActionPerformed
          this.Visor.setText(this.Visor.getText() + ".");
        //this.digito1 = Long.parseLong(this.Visor.getText());
         this.Visor.setText("");
    }//GEN-LAST:event_DecimalActionPerformed

    private void Numero_0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Numero_0ActionPerformed
        this.Visor.setText(this.Visor.getText() + "0");

    }//GEN-LAST:event_Numero_0ActionPerformed

    private void BorrrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BorrrarActionPerformed

        this.Visor.setText("");

    }//GEN-LAST:event_BorrrarActionPerformed

    private void Numero_9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Numero_9ActionPerformed
        // TODO add your handling code here:
        this.Visor.setText(this.Visor.getText() + "9");

    }//GEN-LAST:event_Numero_9ActionPerformed

    private void MultiplicacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MultiplicacionActionPerformed
        // TODO add your handling code here:
        this.digito1 = Long.parseLong(this.Visor.getText());
        this.funcion = "*";
        this.Visor.setText("");

    }//GEN-LAST:event_MultiplicacionActionPerformed

    private void DivisionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DivisionActionPerformed
        // TODO add your handling code here:
        this.digito1 = Long.parseLong(this.Visor.getText());
        this.funcion = "/";
        this.Visor.setText("");
    }//GEN-LAST:event_DivisionActionPerformed


    private void IgualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IgualActionPerformed
        // TODO add your handling code here:
        this.digito2 = Long.parseLong(this.Visor.getText());
        
        if (this.funcion.equals("+")) {

            this.Visor.setText(Long.toString(this.digito1 + this.digito2));
        }
        if (this.funcion.equals("-")) {

            this.Visor.setText(Long.toString(this.digito1 - this.digito2));
        }
        if (this.funcion.equals("*")) {

            this.Visor.setText(Long.toString(this.digito1 * this.digito2));

        }
        if (this.funcion.equals("/")) {

            this.Visor.setText(Long.toString(this.digito1 / this.digito2));

        }
        
        if (this.funcion.equals(".")) {
            
           this.digito1 = Long.parseLong(this.Visor.getText());  
            if (this.funcion.equals("+")) {
                
                aux=digito1;
                
                  this.digito2 = Long.parseLong(this.Visor.getText());
                long b= (long) (this.aux + this.digito2);
                float a = (float)b; 
                this.Visor.setText(Float.toString(a));
;
            }
            if (this.funcion.equals("-")) {

                this.Visor.setText(Float.toString(this.digito1 - this.digito2));
            }

            if (this.funcion.equals("*")) {

                this.Visor.setText(Float.toString(this.digito1 * this.digito2));
            }

            if (this.funcion.equals("/")) {

                this.Visor.setText(Float.toString(this.digito1 / this.digito2));
            }

        }

    }//GEN-LAST:event_IgualActionPerformed

    private void RestaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RestaActionPerformed
        // TODO add your handling code here:
        this.digito1 = Long.parseLong(this.Visor.getText());
        this.funcion = "-";
        this.Visor.setText("");

    }//GEN-LAST:event_RestaActionPerformed

    private void SumaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SumaActionPerformed
        // TODO add your handling code here:
        this.digito1 = Long.parseLong(this.Visor.getText());
        this.funcion = "+";
        this.Visor.setText("");
    }//GEN-LAST:event_SumaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CalculadoraSimple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CalculadoraSimple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CalculadoraSimple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CalculadoraSimple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CalculadoraSimple().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Borrrar;
    private javax.swing.JButton Decimal;
    private javax.swing.JButton Division;
    private javax.swing.JButton Igual;
    private javax.swing.JButton Multiplicacion;
    private javax.swing.JButton Numero_0;
    private javax.swing.JButton Numero_1;
    private javax.swing.JButton Numero_2;
    private javax.swing.JButton Numero_3;
    private javax.swing.JButton Numero_4;
    private javax.swing.JButton Numero_5;
    private javax.swing.JButton Numero_6;
    private javax.swing.JButton Numero_7;
    private javax.swing.JButton Numero_8;
    private javax.swing.JButton Numero_9;
    private javax.swing.JButton Resta;
    private javax.swing.JButton Suma;
    private javax.swing.JLabel Visor;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
