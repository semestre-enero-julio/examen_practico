package ConvertidorKm_Millas;

/**
 *
 * @author Dreed
 */
public class Convertidor extends javax.swing.JFrame {

    public boolean Km = false;
    public boolean Mp = false;

    public Convertidor() {
        initComponents();
        setSize(458, 250);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        TituloKM = new javax.swing.JLabel();
        SliderKilometros = new javax.swing.JSlider();
        SliderMillas = new javax.swing.JSlider();
        LabelKM = new javax.swing.JLabel();
        LabelMillas = new javax.swing.JLabel();
        TituloMillas = new javax.swing.JLabel();

        jLabel4.setText("jLabel4");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TituloKM.setText("Kilometros Por Hora");
        getContentPane().add(TituloKM, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 10, 130, 21));

        SliderKilometros.setMaximum(300);
        SliderKilometros.setValue(0);
        SliderKilometros.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                SliderKilometrosStateChanged(evt);
            }
        });
        SliderKilometros.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                SliderKilometrosMouseEntered(evt);
            }
        });
        getContentPane().add(SliderKilometros, new org.netbeans.lib.awtextra.AbsoluteConstraints(118, 47, -1, -1));

        SliderMillas.setMaximum(186);
        SliderMillas.setValue(0);
        SliderMillas.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                SliderMillasStateChanged(evt);
            }
        });
        SliderMillas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                SliderMillasMouseEntered(evt);
            }
        });
        getContentPane().add(SliderMillas, new org.netbeans.lib.awtextra.AbsoluteConstraints(118, 130, -1, -1));
        getContentPane().add(LabelKM, new org.netbeans.lib.awtextra.AbsoluteConstraints(199, 79, -1, -1));
        getContentPane().add(LabelMillas, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 170, -1, -1));

        TituloMillas.setText("Millas");
        getContentPane().add(TituloMillas, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 110, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SliderKilometrosStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_SliderKilometrosStateChanged
        if (Km) {
            SliderMillas.setValue((int) (this.SliderKilometros.getValue() * 0.621371));
        }
        LabelKM.setText(String.valueOf(this.SliderKilometros.getValue()) + " KM/H");

    }//GEN-LAST:event_SliderKilometrosStateChanged

    private void SliderMillasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_SliderMillasStateChanged
        if (Mp) {
            SliderKilometros.setValue((int) (this.SliderMillas.getValue() * 1.60934));
        }
        LabelMillas.setText(String.valueOf(this.SliderMillas.getValue()) + " MPH");
    }//GEN-LAST:event_SliderMillasStateChanged

    private void SliderKilometrosMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SliderKilometrosMouseEntered

        Km = true;
        Mp = false;

    }//GEN-LAST:event_SliderKilometrosMouseEntered

    private void SliderMillasMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SliderMillasMouseEntered
        Km = false;
        Mp = true;
    }//GEN-LAST:event_SliderMillasMouseEntered

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Convertidor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Convertidor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Convertidor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Convertidor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Convertidor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelKM;
    private javax.swing.JLabel LabelMillas;
    private javax.swing.JSlider SliderKilometros;
    private javax.swing.JSlider SliderMillas;
    private javax.swing.JLabel TituloKM;
    private javax.swing.JLabel TituloMillas;
    private javax.swing.JLabel jLabel4;
    // End of variables declaration//GEN-END:variables
}
